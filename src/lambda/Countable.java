package lambda;

/* Funktionsinterfaces haben eine abstrakte Funktion und
 * koennen mehrere statische und defaultfunktionen haben.
 * default kann nur mit einer Klasseninstanz aufgerufen werden
 */
@FunctionalInterface
public interface Countable {
	
	boolean isCountable(int var);
	
	default void printDefault(){
		System.out.println("default function in functional interface");
	}
	
	static void printStatic(){
		System.out.println("static function in functional interface");
	}

}
