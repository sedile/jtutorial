package lambda;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.function.Function;
import java.util.function.BiFunction;
import java.util.function.BinaryOperator;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class LambdaExpr {
	
	public void simple(){
		/* Lambdaausdruck : (param) -> {body}; */
		Countable c = (int value) -> { return true;};
		boolean erg = c.isCountable(1);
        System.out.println(erg);
	}
	
	public void collectionLambda(){
		List<Integer> list = new ArrayList<>();
		list.add(2);
		list.add(1);
		list.add(0);
		list.add(-1);
		list.add(-2);
		
		/* Beispiel : Lambdas auf Collections anwenden,
		 * mithilfe von Funktionsinterfaces */
		list.forEach(zahl -> System.out.println(zahl));
		list.removeIf((Integer zahl) -> zahl.intValue() < 0);
		System.out.println(list);
	}
	
	public void streamLambda(){
		List<String> list = new ArrayList<>();
		list.add("Bonn");
		list.add("Dortmund");
		list.add("Bochum");
        list.add("Bochum");
		list.add("Aachen");
		list.add("Xanten");
		
        /* Stream erstellen und Funktionen anwenden.
         * Bei einem Stream wird Element fuer Element bearbeitet. */
		Stream<String> stringStream = list.stream();

		/* Fuehrt Aenderungen aus und speichert Sie in ein neuen Stream,
           evtl. eines anderen Rueckgabetypes */
        Stream<String> upper = stringStream.map(String::toUpperCase);
        
        /* Filtert ein Stream nach Bedingungen */
        Predicate<String> onlyB = wort -> wort.startsWith("B"); // Bedingung 1
        Predicate<String> lesserThanFive = wort -> wort.length() < 5; // Bedingung 2

        Stream<String> b = upper.filter(onlyB.and(lesserThanFive));
        b.forEach(System.out::println);

        /* Auch als Verschachtelungen */
        list.stream().map(wort -> wort.toUpperCase())
            .filter(onlyB.and(lesserThanFive))
            .forEach(System.out::println);

        /* reduce-Beispiel (Einfach) */
        String all = list.stream()
            .distinct()
            .reduce((x,y) -> x.concat(y))
            .get();
        System.out.println(all);

        /* reduce-Beispiel (Komplex) */
        int chars = list.stream()
            .distinct()
            
            /* Parameter 1 = int, da Parameter 3 auch int ist
             * Parameter 2 = Lambda
             * Parameter 3 = Funktion welche auf Lambda angewendet werden soll */
            .reduce(0, (sum, word) -> sum + word.length(), Integer::sum);
        System.out.println(chars);

        /* ein Stream als Liste umwandeln */
        List<String> result = list.stream()
            .distinct()
            .filter(onlyB)
            .collect(Collectors.toList());
        System.out.println(result.size());
        
        /* Optional, um NullPointer zu verhindern */
        Optional<String> first = list.stream().findFirst();
        		
	}
	
	public void methodenReferenzen(){
		/* Wenn die Parameterliste und RÃ¼ckgabe gleich sind koennen
		 * Methodenreferenzen benutzt werden */
		Function<String, Integer> fkt = s -> s.length();
		fkt = String::length; // Kurzschreibweise
	}
	
	public void parallelStreams(){
		final int MAX = 500;
		List<Integer> list = Stream.iterate(1, x -> x + 1)
				.limit(MAX)
				.collect(Collectors.toList());
		BiFunction<Integer, Integer, Integer> bi = Integer::sum;
		BinaryOperator<Integer> bo = Integer::sum;
		int sum = list.parallelStream().reduce(0, bi, bo);
		System.out.println(sum);		
	}
	
	public static void main(String[] args){
		LambdaExpr lambda = new LambdaExpr();
		lambda.simple();
		lambda.collectionLambda();
		lambda.methodenReferenzen();
	}
}
