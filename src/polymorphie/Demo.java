package polymorphie;

import polymorphie.item.*;
import polymorphie.lager.Lager;

public class Demo {
	
	private Lager l;
	
	public static void main (String[] args){
		new Demo();
	}
	
	Demo(){
		l = new Lager();
		bestellen();
		kaufen();
		
		int waren = l.getAnzahlWaren();
		double wert = l.getLagerwert();
		System.out.println(waren+" | "+wert);

	}
	
	private void bestellen(){
		l.addItem(new Tisch("Holztisch", 49.95));
		l.updateItem(0).setMaterial("Holz");
		l.addItem(new Tisch("Plastiktisch", 29.95));
		l.updateItem(1).setMaterial("Plastik");
		l.addItem(new Tisch("Luxustisch (Erothin)", 699.95));
		l.updateItem(2).setMaterial("Pechblende");
		l.addItem(new Stuhl("Holzstuhl", 59.95));
		l.updateItem(3).setMaterial("Holz");
		l.addItem(new Stuhl("Gartenstuhl", 45.09));
		l.updateItem(4).setMaterial("Plastik");
		l.addItem(new Kissen("Stoffkissen (Schnuffel)", 9.95));
		l.updateItem(5).setMaterial("Stoff");
		l.addItem(new Kissen("Seidenkissen", 119.19));
		l.updateItem(6).setMaterial("Seide");
	}
	
	private void kaufen(){
		Item i = l.getItem(5);
		System.out.println("Kunde hat gekauft : ");
		i.getItemInfo();
	}

}
