package polymorphie.item;

public abstract class Item {
	
	private String bezeichnung;
	private double preis;
	
	public Item(String n, double p){
		bezeichnung = n;
		preis = p;
	}
	
	public void getItemInfo(){
		String s = new String("Name : "+bezeichnung+" PREIS : "+preis+" MATERIAL : "+getMaterial());
		System.out.println(s);
	}
	
	protected void setNeuerPreis(double neuerPreis){
		if ( preis != neuerPreis ){
			preis = neuerPreis;
		}
	}
	
	public double getPreis(){
		return preis;
	}
	
	public abstract void setMaterial(String material);
	
	public abstract String getMaterial();

}
