package polymorphie.item;

public class Kissen extends Item {
	
	private String material;
	
	public Kissen(String name, double preis){
		super(name,preis);
		material = "";
	}

	@Override
	public void setMaterial(String material) {
		this.material = material;
	}

	@Override
	public String getMaterial() {
		return material;
	}

}
