package polymorphie.lager;

import java.util.ArrayList;

import polymorphie.item.Item;

public class Lager implements LagerOps {
	
	private ArrayList<Item> lagerbestand;
	
	public Lager(){
		lagerbestand = new ArrayList<Item>();
	}
	
	public void druckeLagerbestand(){
		for(int n = 0; n < lagerbestand.size(); n++){
			lagerbestand.get(n).getItemInfo();
		}
	}

	@Override
	public void addItem(Item item) {
		lagerbestand.add(item);
	}

	@Override
	public Item getItem(int index) {
		Item raus = lagerbestand.get(index);
		lagerbestand.remove(index);
		return raus;
	}
	
	public Item updateItem(int index){
		return lagerbestand.get(index);
	}

	@Override
	public double getLagerwert() {
		double wert = 0.00;
		for(int n = 0; n < lagerbestand.size(); n++){
			wert += lagerbestand.get(n).getPreis();
		}
		return wert;
	}

	@Override
	public int getAnzahlWaren() {
		return lagerbestand.size();
	}

}
