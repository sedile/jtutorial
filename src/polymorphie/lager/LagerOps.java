package polymorphie.lager;

import polymorphie.item.Item;

public interface LagerOps {
	
	public void addItem(Item item);
	
	public Item getItem(int index);
	
	public double getLagerwert();
	
	public int getAnzahlWaren();
}
