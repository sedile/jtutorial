package assoziation;

class West {
	
	private Zentrum zentrum;
	
	public West(){}
	
	/* Uebergebe die Referenz zu Zentrum */
	void verbindeMitZentrum(Zentrum zentrum){
		this.zentrum = zentrum;
	}
	
	void drucke(){
		System.out.println("WEST : 1 zu n bidirektional");
		zentrum.druckeWestZentrum();
	}

}
