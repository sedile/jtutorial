package assoziation;

class Zentrum {
	public static void main (String[] args){
		new Zentrum();
	}
	
	Zentrum(){
		/* unidirektional 1 zu 1
		 * Nur Zentrum kennt Nord
		 */
		Nord nord = new Nord();
		nord.drucke();
		
		System.out.println("-----");
		
		/* bidirektional 1 zu 1
		 * Zentrum kennt Ost und Ost kennt Zentrum
		 */
		Ost ost = new Ost();
		ost.verbindeMitZentrum(this);
		ost.drucke();
		
		System.out.println("-----");
		
		/* unidirektional 1 zu n
		 * Nur Zentrum kennt n SÃ¼d
		 */
		Sued[] sued = new Sued[3];
		for(int i = 0; i < sued.length; i++){
			sued[i] = new Sued();
			sued[i].drucke();
		}
		
		System.out.println("-----");
		
		/* bidirketional 1 zu n
		 * Zentrum kennt n West und umgekehrt
		 */
		West[] west = new West[3];
		for(int j = 0; j < west.length; j++){
			west[j] = new West();
			west[j].verbindeMitZentrum(this);
			west[j].drucke();
		}
	}
	
	void druckeOstZentrum(){
		System.out.println("ZENTRUM : 1 zu 1 bidirektional");
	}
	
	void druckeWestZentrum(){
		System.out.println("ZENTRUM : 1 zu n bidirektional");
	}

}
