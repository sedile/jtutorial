package threading;

import java.util.concurrent.CountDownLatch;
import java.util.logging.Level;
import java.util.logging.Logger;

/* Beispiel mit CountDownLatch
 * wenn ein Thread, z.B. main-Thread auf ein oder
 * mehrere Threads warten muss
 */
public class Service implements Runnable {
	
	private final String NAME;
	private final int TIME_TO_START;
	private final CountDownLatch LATCH;
	
    public Service(String name, int timeToStart, CountDownLatch latch){
        NAME = name;
        TIME_TO_START = timeToStart;
        LATCH = latch;
    }
	
    @Override
    public void run(){
    	try {
    		Thread.sleep(TIME_TO_START);
    	} catch (InterruptedException ie){
            Logger.getLogger(Service.class.getName()).log(Level.SEVERE, null, ie);
        }
    	
        System.out.println( NAME + " wurde gestartet");
        LATCH.countDown(); /* reduziere den Wert des  CountDownLatch um 1 */
    }
}

/* In die main kopieren, zum testen
// Erzeuge ein Countdownlatch und alle Threads die der main-Thread zum
// weiterarbeiten benoetigt und starte diese
final CountDownLatch latch = new CountDownLatch(3);
Thread cacheService = new Thread(new Service("Cache-Service", 1000, latch));
Thread alertService = new Thread(new Service("Alarm-Service", 1000, latch));
Thread validationService = new Thread(new Service("Authentifizierungs-Service", 1000, latch));

// Initialisiere die unterschiedlichen Services
cacheService.start();
alertService.start();
validationService.start();

try{
	// main-Thread wartet, bis alle drei 'Services' gestartet sind
	latch.await();  
    System.out.println("Alle Services gestartet, System bereit");
}catch(InterruptedException ie){
    ie.printStackTrace();
}
*/
