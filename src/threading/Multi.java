package threading;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;


public class Multi {

	private ExecutorService executor;
	private List<Map<Integer, Integer>> list;
	
	public Multi(){
		list = new ArrayList<Map<Integer, Integer>>();
		
		Map<Integer, Integer> a = new HashMap<Integer,Integer>();
		a.put(1, 2);
		a.put(2, 3);
		a.put(3, 4);
		
		Map<Integer, Integer> b = new HashMap<Integer,Integer>();
		b.put(2, 2);
		b.put(4, 3);
		
		Map<Integer, Integer> c = new HashMap<Integer,Integer>();
		c.put(0, 2);
		c.put(3, 4);
		
		list.add(a);
		list.add(b);
		list.add(c);
		
		executor = Executors.newFixedThreadPool(2);
	}
	
	public void startRunnable(){
		/* erstelle eine Liste voller Aufgaben/Threads */
		List<Thread> threadlist = new ArrayList<Thread>();
		for(int i = 0; i < list.size(); i++){
			threadlist.add(new Thread(new Consumer(list.get(i))));
		}
		
		/* Starte alle Aufgaben mit dem ExecutorService */
		for(int i = 0; i < threadlist.size(); i++){
			executor.execute(threadlist.get(i));
		}
		
		/* Es sollen keine weiteren Threads erzeugt werden */
		executor.shutdown();
		try {
			executor.awaitTermination(60, TimeUnit.SECONDS);
		} catch (InterruptedException e) {
			executor.shutdownNow();
			e.printStackTrace();
		}
	}
	
	public void startCallable(){
		/* erste eine Liste voller Aufgaben/Threads */
		List<Future<Map<Integer,Integer>>> futurelist = new ArrayList<Future<Map<Integer,Integer>>>();
		for(int i = 0; i < list.size(); i++){
			final Future<Map<Integer,Integer>> future = executor.submit(new Task(list.get(i),i));
			futurelist.add(future);
		}
		
		/* Starte alle Aufgaben mit dem ExecutorService */
		List<Map<Integer, Integer>> ret = new ArrayList<Map<Integer, Integer>>();
		for(Future<Map<Integer,Integer>> f : futurelist){
			try {
				ret.add(f.get(5, TimeUnit.SECONDS));
			} catch (InterruptedException | ExecutionException | TimeoutException e) {
				ret.add(new HashMap<Integer,Integer>());
				e.printStackTrace();
			}
		}
		
		/* Gebe Ergebnisse aus */
		for(int i = 0; i < ret.size(); i++){
			for(Map.Entry<Integer, Integer> elem : ret.get(i).entrySet()){
				System.out.println(elem.getKey()+" "+elem.getValue());
			}
		}
		
		/* Es sollen keine weiteren Threads erzeugt werden */
		executor.shutdown();
		try {
			executor.awaitTermination(60, TimeUnit.SECONDS);
		} catch (InterruptedException e) {
			executor.shutdownNow();
			e.printStackTrace();
		}
	}

	private class Consumer implements Runnable {
		
		private Map<Integer, Integer> temp;
		
		public Consumer(Map<Integer, Integer> map){
			temp = map;
		}
		
		private void addValues(){
			int sum = 0;
			for(Map.Entry<Integer, Integer> elem : temp.entrySet()){
				sum += elem.getValue();
			}
			System.out.println(Thread.currentThread().getName() + " Summe : " + sum);
		}

		@Override
		public void run() {
			addValues();
		}
		
	}
	
	private class Task implements Callable<Map<Integer,Integer>> {
		
		private Map<Integer, Integer> temp;
		private Map<Integer, Integer> result;
		private final int IDX;
		
		public Task(Map<Integer, Integer> map, int idx){
			temp = map;
			IDX = idx;
			result = new HashMap<Integer,Integer>();
		}
		
		private void createMap(){
			for(Map.Entry<Integer, Integer> elem : temp.entrySet()){
				if( elem.getKey() % 2 == 0){
					result.put(elem.getKey(), elem.getValue());
				}
			}
		}

		@Override
		public Map<Integer, Integer> call() throws Exception {
			createMap();
			return result;
		}
		
	}
	
	public static void main(String[] args) {
	    Multi m = new Multi();
	    m.startRunnable();
	    //m.startCallable();
	}
	
}