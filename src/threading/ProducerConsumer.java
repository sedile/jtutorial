package threading;

import java.util.Random;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;

/* Beispiel mit 'ProducerConsumer' */
public class ProducerConsumer {
	
	/* concurrentutilities bevorzugen */
	private BlockingQueue<Integer> queue;
	
	public ProducerConsumer(){
		queue = new ArrayBlockingQueue<Integer>(10);
	}
	
	/* Nimmt Items aus der Queue */
	public void consumer() throws InterruptedException {
		Random random = new Random();
		
		while(true){
			queue.put(random.nextInt(100));
		}
	}
	
	/* Es wird solange ein Item hinzugefuegt, bis die Queue
	 * den Wert 10 erreicht hat. Werden Items von consumer
	 * entnommen, so werden wieder Items hinzugefuegt */
	public void producer() throws InterruptedException {
		Random random = new Random();
		
		while(true){
			Thread.sleep(100);
			
			if( random.nextInt(10) == 0){
				Integer value = queue.take();
				System.out.println("Value : " + value + " Queuesize : " + queue.size());
			}
		}
	}
}

/* In die main kopieren, zum testen
final ProducerConsumer pc = new ProducerConsumer();

Thread one = new Thread(new Runnable(){
	
	@Override
	public void run(){
		try {
			pc.producer();
		} catch (InterruptedException ie ){
			System.err.println(ie.toString());
		}
	}
});

Thread two = new Thread(new Runnable(){
	
	@Override
	public void run(){
		try {
			pc.consumer();
		} catch (InterruptedException ie ){
			System.err.println(ie.toString());
		}
	}
});

one.start();
two.start();

try {
	one.join();
	two.join();
} catch (InterruptedException ie){
	System.err.println(ie.toString());
}
*/
