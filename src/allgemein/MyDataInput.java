package allgemein;

import java.io.BufferedOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.net.Socket;

public class MyDataInput extends Thread {

	private final int PORT = 9998;
	private final String HOST = "127.0.0.1";
	private final String DATA = "/home/sebastian/Schreibtisch/testx";
	private final int SIZE = 3197613;
	
	public void stopDataServer(){
		interrupt();
	}
	
	@Override
	public void run(){
		int bytesRead;
	    int current = 0;
	    FileOutputStream fos = null;
	    BufferedOutputStream bos = null;
	    Socket sock = null;
	    try {
	    	sock = new Socket(HOST, PORT);
	    	System.out.println("Connecting...");

	    	// receive file
	    	byte [] mybytearray  = new byte [SIZE];
	    	ObjectInputStream is = new ObjectInputStream(sock.getInputStream());
	    	fos = new FileOutputStream(DATA);
	    	bos = new BufferedOutputStream(fos);
	    	bytesRead = is.read(mybytearray,0,mybytearray.length);
	    	current = bytesRead;

	    	do {
	    		bytesRead =
	    				is.read(mybytearray, current, (mybytearray.length-current));
	    		if(bytesRead >= 0) current += bytesRead;
	    	} while(bytesRead > -1);

	    	bos.write(mybytearray, 0 , current);
	    	bos.flush();
	    	System.out.println("File " + DATA
	          + " downloaded (" + current + " bytes read)");
	    } catch ( Exception e ){
	    	e.printStackTrace();
	    } finally {
	    		if (fos != null) {
	    			try {
						fos.close();
					} catch (IOException e) {
						e.printStackTrace();
					}
	    		}
	    		if (bos != null) {
	    			try {
						bos.close();
					} catch (IOException e) {
						e.printStackTrace();
					}
	    		}
	    		if (sock != null) {
	    			try {
						sock.close();
					} catch (IOException e) {
						e.printStackTrace();
					}
	    		}
	    }
	}

	public static void main (String [] args ) throws Exception {
	  new MyDataInput().start();
  }
}