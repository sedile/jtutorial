package allgemein;

class Zugriff {
	public static void main (String[] args){
		
		Zugriff z = new Zugriff();
		
		/* Private-Zugriff */
		z.setPrivatesAttribut(1);
		int pri = z.getPrivatesAttribut();
		
		/* Protected-Zugriff (Sinnvoll bei Vererbung) */
		int pro = z.getSchutzAttribut();
		
		/* Default-Zugriff */
		z.drei = 3;
		int def = z.drei;
		
		/* Public-Zugriff (ausserhalb eines Paktes) */
		z.vier = 4;
		int pub = z.vier;
		
		/* Static-Zugriff ohne Objekte */
		Zugriff.fuenf = 5;
		int sta = Zugriff.fuenf;
		
		System.out.print(pri+" "+pro+" "+def+" "+pub+" "+sta);
		
	}
	
	private int eins;
	protected final int ZWEI = 2;
	int drei;
	public int vier;
	static int fuenf;

	/* Schnittstelle zum setzen eines privaten Attributes */
	public void setPrivatesAttribut(int wert){
		eins = wert;
	}
	
	/* Schnittstelle zum erfahren eines privaten Attributes nur fÃ¼r das
	 * aktuelle Paket */
	int getPrivatesAttribut(){
		return eins;
	}
	
	/* Nur erbende Klassen und Klassen des selben Paketes koennen zugreifen */
	protected int getSchutzAttribut(){
		return ZWEI;
	}
	
	int setzeUndBekommeWert(int wert){
		drei = wert;
		return drei;
	}
}
