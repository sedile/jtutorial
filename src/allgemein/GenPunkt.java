package allgemein;

import java.io.Serializable;

/* Mit extends kann der generische Datentyp eingeschraenkt werden,
 * d.h. es koennen nur Typen uebergeben werden die von z.B. Number
 * erben
 * Mit einem & kann ein Datentyp weiter eingeschraenkt werden
 */

public class GenPunkt<T extends Number & Serializable> {
	
	private T x, y;

	public GenPunkt(T posx, T posy){
		x = posx;
		y = posy;
	}

	public T getX(){
		return x;
	}
	
	public T getY(){
		return y;
	}
	
	/* Da Integer eine Subklasse von Number ist
	 * kann zu Integer gecastet werden
	 */
	public Integer intValue(){
		return (Integer) x;
	}
	
	/* Moechte man generische Typen an Funktionen binden, so wird
	 * der generische Typ vor dem Rueckgabewert geschrieben.
	 */
	public <U extends Number> void printGenType(U u){
		System.out.println(prepareString(u));
	}
	
	/* Das selbe mit Rueckgabeparameter */
	private <U extends Number> String prepareString(U u){
		return u.getClass().getName();
	}

	public String pointToString(){
		return new String("("+getX()+","+getY()+")");
	}

}
