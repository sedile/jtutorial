package allgemein;

/*
   [1] = Konstruktor

         public class Hilfe {
	      	byte alter;
	       	int level;
	
        	Hilfe (byte alter){
		     	this.alter = alter;
		    }
		 }
		 
		 Ein Konstruktor erzeugt ein Objekt vom Typ der Klasse. In ihm
		 koennen z.B. diverse Instanzattribute initialisiert werden

   [2] = Ueberladen
   
   	     public static int methode(int x, int y){}
   	     public static int methode(int x, int y, int z){}
   	     
   	     int c = methode(x,y);
    
    	 Eine Methode zu ueberladen, bedeutet, dass eine Methode dieselbe
    	 bezeichnung hat, aber eine andere Anzahl/Typen von Parametern
		 Mit 'c' wird die Methode mit zwei Parametern aufgerufen


  [3] = Getter/Setter 
  
  		void setX (byte x){
          	this.x = x; 
        }

		byte getX (){
	  		return x;
	  	}
  
  		Um das Geheimnisprinzip/Kapselung zu bewahren, sind Setter/Getter-
  		Methoden gut geeignet, um von aussen auf private Attribute zugreifen
  		zu koennen.

  [4] = Interface : 
  
  		public interface Name {
  		
  			final int x = 5;
  			
  			public void test();
  			
  		}
  		
  		Ein Interface ist eine Sammlung von finalen Attributen und abstrakten
  		Methoden, die in der Klasse Ueberschrieben (Override) werden, die das
  		jeweilige Interface implementieren.
  		
  		Einsatzgebiete : Strukturierung, Vorgaben, Polymorphie

  [5] = Throw und catch/try

        public Mensch (int alter, int iq) throws Exception {
        	if (alter < 0){
          		throw new Exception("Fehler");
            	}
        }

		Eine Exception kann gewurfen und behandelt werden, falls ein Fehler
		auftreten kann. Diese Behandlung erfolgt in der catch-Klausel

  [6] Polymorphismus : 
  
  		Polymorphie bedeutet, dass eine Superklasse auch auf ihre Subklassen zugreifen kann.
  
  		Beispiel : Tasche[] super = new Super[3];
  				   super[0] = new Trank();
  				   super[1] = new Schwert();
  				   super[2] = new Schild();
  				   
  		Dies geht nur, wenn Trank, Schwert und Schild von der Superklasse erben
  
  
  Eine Objektvariable der Superklasse kann sowohl auf Objekte der
                       Superklasse als auch auf Objekte ihrer Unterklassen referenzieren.
                       
                       (1) In Superklasse (abstract/interface) : schreibe methodeX()
                       (2) In Subklassen : Ueberschreibe methodeX()
                       (3) In Main-Klasse : Superklasse ref = new Unterklasse();
                       (4) In Main-Klasse : ref.methodeX();
                       
  [7] Immutable = unveraenderbare Objekte 
  		- eigene Klassen mit nur getterfunktionen / final variablen
  		- werden attribute veraendert, dann neue immutable objekte erstellen

*/
