package allgemein;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class SQLConnect {
	
	private Connection con;
	
	public SQLConnect(){
		con = null;
	}
	
	public void connectToHSQLDatabase(){
		try {
			Class.forName("org.hsqldb.jdbcDriver");
		} catch ( ClassNotFoundException cnfe ){
			System.err.println(cnfe.toString());
		}

		try {
			con = DriverManager.getConnection("jdbc:hsqldb:file:/home/sebastian/Dokumente: shutdown=true",
					"root",
					"root");
		} catch (SQLException e) {
			System.err.println(e.toString());
		}
	}
	
	public ResultSet executeSQL(String query){
		try {
			Statement stmt = con.createStatement();
			ResultSet result = stmt.executeQuery(query);
			return result;
		} catch ( SQLException e ){
			System.err.println(e.toString());
		}
		return null;
	}
	
	public void printResultSet(ResultSet resultSet) {
		if ( resultSet == null){
			return;
		}
		
		try {
			/* Jedes Tupel durchlaufen */
			while( resultSet.next() ){
				/* Tupelattribute */
				int maxCol = resultSet.getMetaData().getColumnCount();
				StringBuilder sb = new StringBuilder();
				for(int i = 1; i < maxCol; i++){
					sb.append(resultSet.getMetaData().getColumnName(i));
					sb.append(" = ");
					sb.append(resultSet.getString(i));
				}
				System.out.println(sb.toString());
			}
		} catch (SQLException e) {
			System.err.println(e.toString());
		}
	}
	
	// Demonstrationsfunktionen //
	public boolean insertCustomer(String firstname, String lastname, String street, String city){
		/* ID ermitteln */
		ResultSet r = executeSQL("SELECT * FROM Customer");
		int index = 0;
		try {
			while( r.next() ){
				if ( r.getInt(1) > index){
					index = r.getInt(1);
				}
			}
			/* ID + 1, da neuer Eintrag */
			index++;
		} catch ( SQLException e){
			System.err.println(e.toString());
			return false;
		} finally {
			try {
				r.close();
			} catch (SQLException ie) {
				System.err.println(ie.toString());
				return false;
			}
		}
		
		try {
			String sql = "INSERT INTO Customer VALUES (?,?,?,?,?)";
			PreparedStatement pre = con.prepareStatement(sql);
			
			// (Attributreihenfolge in der DB, Wert)
			pre.setInt(1, index);
			pre.setString(2, firstname);
			pre.setString(3, lastname);
			pre.setString(4, street);
			pre.setString(5, city);
			pre.executeUpdate();
			pre.close();
			return true;
		} catch ( SQLException e){
			System.err.println(e.toString());
			return false;
		}
	}
	
	// Simulation : Ein Kunde kauft etwas ein
	public boolean buy(int customer, int betrag){
		if ( customer == - 1){
			return false;
		}
		
		try {
			PreparedStatement pre = null;
			String sql = "INSERT INTO Rechnung (CustomerID, BETRAGE) VALUES(?,?)";
			
			pre = con.prepareStatement(sql);
			pre.setInt(1, customer);
			pre.setInt(2, betrag);
			pre.executeUpdate();
			pre.close();
			return true;
		} catch ( SQLException e){
			System.err.println(e.toString());
			return false;
		}
	}
	
	// Simulation : Ein Kunde kauft etwas ein
	public boolean buy(String firstname, String lastname, int betrag) throws SQLException{
		return buy(getCustomerID(firstname, lastname), betrag);
	}
	
	// ID eines Kunden anhand seines Vor- und Nachnamen ermitteln
	public int getCustomerID(String firstname, String lastname) throws SQLException{
		String sql = "SELECT ID, Firstname, Lastname FROM Customer";
		ResultSet r = executeSQL(sql);
		while( r.next() ){
			if( firstname.equals(r.getString(2)) && lastname.equals(r.getString(3))){
				return r.getInt(1);
			}
		}
		return -1;
	}
	
	// Gibt alle Rechnungen mit Kunden aus (Join)
	public ResultSet getPayments(){
		return executeSQL("SELECT Firstname, Lastname, Betrage FROM Customer INNER JOIN Rechnung ON Customer.ID = Rechnung.CustomerID");
	}
	
	
	/* Schliesst die Verbindung am Ende der Anwendung */
	public void closeConnection(){
		try {
			con.close();
		} catch (SQLException e){
			System.err.println(e.toString());
		}
	}

	public static void main(String[] args){
		SQLConnect entry = new SQLConnect();
		entry.connectToHSQLDatabase();
		
		/* diverse Aufrufe */
		entry.insertCustomer("Daniel","Dresbach","Apfelallee 5","Erothin");
		ResultSet r = entry.executeSQL("SELECT * FROM Customer");
		try {
			entry.buy(entry.getCustomerID("Julia","Peterson"), 25);
		} catch (SQLException e) {
			System.err.println(e.toString());
		}
		entry.printResultSet(r);
		
		ResultSet r2 = entry.getPayments();
		entry.printResultSet(r2);
		
		entry.closeConnection();
	}

}

// Datenhaltung in Java 7