package allgemein;

import java.util.Scanner;

class Einlesen {
	public static void main(String[] args){
		short a,b,n;
		Scanner s = new Scanner(System.in); //Zum Einlesen von Daten
		System.out.println("Geben Sie die erste Zahl ein.");
		a = s.nextShort();  // Scanner als Variable schreiben
		System.out.println("Geben Sie die zweite Zahl ein.");
		b = s.nextShort();
		System.out.println("Die Zahlen lauten :" + a + " und " + b + ".");
		System.out.println("Geben Sie abschliessend eine weitere Zahl X ein");
		n = s.nextShort();
		s.close();

		for (short i = 1; i <= n; i++){
        if ( n < i){
        	  System.out.println("Kein Schleifendurchlauf");
          }else{
        	  System.out.println(i + " Schleifendurchlaeufe");
          }
        } // for-Schleife
	}
}

/* wird ein String (Zeichenkette) eingelesen, dann schreibt man
  <Variablenname> = <Scannername>.next(); ohne Datentyp */
